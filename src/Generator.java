import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {
    private int generationNumber;
    private int idSequence;

    private final Random rnd = new Random(System.currentTimeMillis() % 335417);

    public Generator(int paramCount, int prevGenerationNumber, int lastId) {
        generationNumber = prevGenerationNumber;
        idSequence = lastId + 1;
    }

    public Generator(int paramCount) {
        this(paramCount, -1 , -1);
    }

    public List<Bot.Config> createFirstGeneration() {
        generationNumber++;

        List<Bot.Config> firstGen = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            firstGen.add(createRandomConfig());
        }
        return firstGen;
    }

    public List<Bot.Config> nextGeneration(List<Bot.Config> winners) {
        generationNumber++;

        List<Bot.Config> generation = new ArrayList<>();

        for (int i = 0; i < winners.size(); i++) {
            Bot.Config winner = winners.get(i);
            generation.add(copy(winner));
            generation.add(mutate(winner));
            if (generationNumber > 10)
                generation.add(mutate(winner));

            for (int j = i + 1; j < winners.size(); j++) {
                generation.add(makeHybrid(winner, winners.get(j)));
            }
        }

        if (generationNumber <= 10) {
            for (int i = 0; i < 3; i++) {
                generation.add(createRandomConfig());
            }
        }
        return generation;
    }

    public int getGenerationNumber() {
        return generationNumber;
    }

    public static int extractId(String id) {
        return Integer.parseInt(id.split("-")[3]);
    }

    private Bot.Config createRandomConfig() {
        Bot.Config config = new Bot.Config(Bot.PARAM_COUNT);
        config.id = String.format("g%04d-%04d-RANDOM______-%05d", generationNumber, generationNumber, nextId());

        for (int i = 0; i < Bot.PARAM_COUNT; i++) {
            config.params[i] = getRandomParam();
        }

        return config;
    }

    private Bot.Config copy(Bot.Config orig) {
        Bot.Config config = new Bot.Config(Bot.PARAM_COUNT);
        System.arraycopy(orig.params, 0, config.params, 0, Bot.PARAM_COUNT);
        String[] idParts = orig.id.split("-");
        int initGen = Integer.parseInt(idParts[1]);
        int id = Integer.parseInt(idParts[3]);
        config.id = String.format("g%04d-%04d-PREV_%05d__-%05d", generationNumber, initGen, id, id);
        return config;
    }

    private Bot.Config mutate(Bot.Config orig) {
        Bot.Config config = new Bot.Config(Bot.PARAM_COUNT);

        for (int i = 0; i < Bot.PARAM_COUNT; i++) {
            float param = randDiff(0.2f) + orig.params[i];

            if (param < -1)
                param = -1;
            if (param > 1)
                param = 1;

            config.params[i] = param;
        }

        int id = extractId(orig.id);
        config.id = String.format("g%04d-%04d-MUTATE_%05d-%05d", generationNumber, generationNumber, id, nextId());
        return config;
    }

    private Bot.Config makeHybrid(Bot.Config par1, Bot.Config par2) {
        Bot.Config config = new Bot.Config(Bot.PARAM_COUNT);

        for (int i = 0; i < Bot.PARAM_COUNT; i++) {
            float param = (par1.params[i] + par2.params[i]) / 2;
            param += randDiff(0.1f);

            if (param < -1)
                param = -1;
            if (param > 1)
                param = 1;

            config.params[i] = param;
        }

        config.id = String.format("g%04d-%04d-H%05d,%05d-%05d", generationNumber, generationNumber,
                extractId(par1.id), extractId(par2.id), nextId());
        return config;
    }

    private float getRandomParam() {
//        float param = randFloat(1.25f);
//
//        if (doWithProbability(0.3f))
//            param = -param;

        return 2 * randFloat(1.0f) - 1;
    }

    private int nextId() {
        return idSequence++;
    }

    private float randFloat(float max) {
        return rnd.nextFloat() * max;
    }

    private boolean doWithProbability(float probability) {
        return rnd.nextFloat() <= probability;
    }

    private float randCoef(float max) {
        float coef = rnd.nextFloat() - 0.5f;
        coef *= 2 * max;
        return coef + 1;
    }

    private float randDiff(float max) {
        float diff = rnd.nextFloat() - 0.5f;
        return diff * 2 * max;
    }
}
