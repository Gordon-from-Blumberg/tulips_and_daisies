import java.util.*;

import static java.lang.Math.*;

class Player {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Bot bot = new Bot(
                +0.877f,
                        +0.975f,
                        +0.144f,
                        -0.640f,
                        +0.400f,
                        +0.876f,
                        -0.257f,
                        +1.130f,
                        +0.548f,
                        +1.219f,
                        +0.304f,
                        +0.299f,
                        -1.002f,
                        +0.120f,
                        +0.746f,
                        +0.217f,
                        +0.712f,
                        +0.074f,
                        +0.559f

        );
        bot.init(in.nextInt(), in.nextInt(),
                in.nextInt(), in.nextInt(), in.nextInt(), in.nextInt(),
                in.next(), in.next());

        final String[] grid = new String[bot.getHeight()];

        while (true) {
            int turnsLeft = in.nextInt(); // The number of turns left in the game
            int yourGold = in.nextInt(); // Your current gold
            int opponentGold = in.nextInt(); // Opponents current gold
            for (int i = 0; i < grid.length; i++) {
                grid[i] = in.next(); // playing field encoded by S - soil, G - grass, R - rocks, T - tulip, D - daisy.
            }

            System.out.println(bot.runTurn(turnsLeft, yourGold, opponentGold, grid));
        }
    }
}

class Bot {
    public static final int COST_COEF_INDEX = 0;
    public static final Map<Character, int[]> COEF_INDEX_MAP = new HashMap<>();
    public static final int[] OWN_FLOWER_COEF_INDEX = new int[] {43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56};
    public static final int[] OPP_FLOWER_COEF_INDEX = new int[] {57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70};
    public static final int HARVESTED_COUNT_COEF_INDEX = 71;
    public static final int HARVESTED_EARNED_COEF_INDEX = 72;
    public static final int HARVESTED_EARNED_BY_OPP_COEF_INDEX = 73;

    public static final char SOIL = 'S';
    public static final char GRASS = 'G';
    public static final char ROCKS = 'R';

    private static final int[] FIB = new int[26];
    private static final int[] FIB_SUM = new int[26];

    static {
        COEF_INDEX_MAP.put(SOIL, new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14});
        COEF_INDEX_MAP.put(GRASS, new int[] {15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28});
        COEF_INDEX_MAP.put(ROCKS, new int[] {29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42});

        calcHarvestedCost();
    }

    public static final int PARAM_COUNT = 74;

    Config config = new Config(PARAM_COUNT);

    private int width;
    private int height;

    private final Map<Character, Integer> costMap = new HashMap<>();

    private char ownFlowerType;
    private char oppFlowerType;

    private Cell[][] grid;

    private int turnsLeft;
    private int gold;
    private int oppGold;

    private PriorityQueue<Cell> priorityQueue;

    private final ProcessResult result = new ProcessResult();

    public Bot() {}

    public Bot(float... params) {
        for (int i = 0; i < params.length; i++) {
            config.params[i] = params[i];
        }
    }

    public void init(int width, int height,
                     int costSoil, int costGrass, int costRocks, int costFlower,
                     String ownFlowerType, String oppFlowerType) {

        this.width = width;
        this.height = height;

        debug("width = %d, height = %d", width, height);

        costMap.clear();
        costMap.put(SOIL, costSoil);
        costMap.put(GRASS, costGrass);
        costMap.put(ROCKS, costRocks);

        this.ownFlowerType = ownFlowerType.toUpperCase().charAt(0);
        this.oppFlowerType = oppFlowerType.toUpperCase().charAt(0);

        costMap.put(this.oppFlowerType, costFlower);
        COEF_INDEX_MAP.put(this.ownFlowerType, OWN_FLOWER_COEF_INDEX);
        COEF_INDEX_MAP.put(this.oppFlowerType, OPP_FLOWER_COEF_INDEX);

        grid = new Cell[width][height];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                grid[x][y] = new Cell(x, y);
            }
        }

        priorityQueue = new PriorityQueue<>(width * height);
    }

    public String runTurn(int turnsLeft, int gold, int oppGold, String[] grid) {
        this.turnsLeft = turnsLeft;
        this.gold = gold;
        this.oppGold = oppGold;

        priorityQueue.clear();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                this.grid[x][y].type = grid[y].charAt(x);
            }
        }

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                char type = this.grid[x][y].type;
                if (type != ownFlowerType && costMap.get(type) <= gold) {
                    this.grid[x][y].priority = processCell(this.grid[x][y]);
                    priorityQueue.add(this.grid[x][y]);
                }
            }
        }

        debug("priority queue contains %d", priorityQueue.size());
        Cell turn = priorityQueue.peek();
        return turn != null ? turn.toString() : "-1 -1";
    }

    public void setAllConfigParams(float value) {
        for (int i = 0; i < PARAM_COUNT; i++) {
            config.params[i] = value;
        }
    }

    public int getHeight() {
        return height;
    }

    public String getName() {
        return config.id;
    }

    private float processCell(Cell cell) {
        int cx = cell.x;
        int cy = cell.y;
        char cellType = grid[cx][cy].type;

        result.reset();

        result.priority -= costMap.get(cellType) * config.params[COST_COEF_INDEX];
        for (Direction dir : Direction.values()) {
            dir.process(cell, this, result);
        }

        float cellPriority = 0;
        int consideredCells = 0;
        for (int x = max(0, cx - 4), xMax = min(width - 1, cx + 4); x <= xMax; x++) {
            for (int y = max(0, cy - 4), yMax = min(height - 1, cy + 4); y <= yMax; y++) {
                if (x != cx || y != cy) {
                    cellPriority += config.params[COEF_INDEX_MAP.get(grid[x][y].type)[getParamIndex(x - cx, y - cy)]];
                    consideredCells++;
                }
            }
        }

        result.priority += cellPriority / consideredCells;

        int canBeHarvestedByMe = 1;
        int canBeHarvestedByOpp = 1;

        for (int i = 0; i < 4; i++) {
            if (result.canBeHarvestedByMe[i] >= 3) canBeHarvestedByMe += result.canBeHarvestedByMe[i];
            if (result.canBeHarvestedByOpp[i] >= 3) canBeHarvestedByOpp += result.canBeHarvestedByOpp[i];
        }

        if (canBeHarvestedByMe > 1) {
            result.priority -= (canBeHarvestedByMe - 1) * config.params[HARVESTED_COUNT_COEF_INDEX];
            result.priority += FIB_SUM[canBeHarvestedByMe] * config.params[HARVESTED_EARNED_COEF_INDEX] * 0.01f;
        }
        if (canBeHarvestedByOpp > 1)
            result.priority += FIB_SUM[canBeHarvestedByOpp] * config.params[HARVESTED_EARNED_BY_OPP_COEF_INDEX];

        return result.priority;
    }

    private int getParamIndex(int dx, int dy) {
        dx = abs(dx);
        dy = abs(dy);
        int min = min(dx, dy);
        int max = max(dx, dy);

        switch (min) {
            case 0:
                return max - 1;
            case 1:
                return max + 3;
            case 2:
                return max + 6;
            case 3:
                return max + 8;
            default:
                return 13;
        }
    }

    public static int getHarvestedCost(int n) {
        return FIB_SUM[n];
    }

    public static void debug(String message, Object... params) {
//        System.err.println(String.format(message, params));
    }

    @Override
    public int hashCode() {
        return config.id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || obj.getClass() != getClass()) return false;

        return config.id.equals(((Bot) obj).config.id);
    }

    private static void calcHarvestedCost() {
        FIB[0] = FIB_SUM[0] = 0;
        FIB[1] = FIB_SUM[1] = 1;

        for (int n = 2; n < 26; n++) {
            FIB[n] = FIB[n - 1] + FIB[n - 2];
            FIB_SUM[n] = FIB_SUM[n - 1] + FIB[n];
        }
    }

    public static class Config {
        final float[] params;
        String id = "id";

        public Config(int paramCount) {
            params = new float[paramCount];
        }
    }

    private static class Cell implements Comparable<Cell> {
        private final int x;
        private final int y;
        private char type;
        private float priority;

        Cell(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(Cell o) {
            return (int) (o.priority - priority);
        }

        @Override
        public String toString() {
            return String.format("%d %d %.3f", y, x, priority);
        }
    }

    public enum Direction {
        NORTH(0, -1, 0) {
            @Override
            protected int maxSteps(int x, int y, int width, int height) {
                return y;
            }
        },

        SOUTH(0, 1, 0) {
            @Override
            protected int maxSteps(int x, int y, int width, int height) {
                return height - 1 - y;
            }
        },

        NORTH_EAST(1, -1, 1) {
            @Override
            protected int maxSteps(int x, int y, int width, int height) {
                return min(width - 1 - x, y);
            }
        },

        SOUTH_WEST(-1, 1, 1) {
            @Override
            protected int maxSteps(int x, int y, int width, int height) {
                return min(x, height - 1 - y);
            }
        },

        EAST(1, 0, 2) {
            @Override
            protected int maxSteps(int x, int y, int width, int height) {
                return width - 1 - x;
            }
        },

        WEST(-1, 0, 2) {
            @Override
            protected int maxSteps(int x, int y, int width, int height) {
                return x;
            }
        },

        SOUTH_EAST(1, 1, 3) {
            @Override
            protected int maxSteps(int x, int y, int width, int height) {
                return min(width - 1 - x, height - 1 - y);
            }
        },

        NORTH_WEST(-1, -1, 3) {
            @Override
            protected int maxSteps(int x, int y, int width, int height) {
                return min(x, y);
            }
        };

        private final int dx;
        private final int dy;
        private final int dirIndex;

        Direction(int dx, int dy, int dirIndex) {
            this.dx = dx;
            this.dy = dy;
            this.dirIndex = dirIndex;
        }

        void process(Cell cell, Bot bot, ProcessResult res) {
            boolean keepCountingMine = true;
            boolean keepCountingOpp = cell.type != bot.oppFlowerType;

            for (int i = 1, steps = min(maxSteps(cell.x, cell.y, bot.width, bot.height), 3); i <= steps; i++) {
                int x = cell.x + i * dx;
                int y = cell.y + i * dy;
                res.priority += bot.config.params[COEF_INDEX_MAP.get(bot.grid[x][y].type)[i - 1]];

                if (keepCountingMine && bot.grid[x][y].type == bot.ownFlowerType)
                    res.canBeHarvestedByMe[dirIndex]++;
                else
                    keepCountingMine = false;

                if (keepCountingOpp && bot.grid[x][y].type == bot.oppFlowerType)
                    res.canBeHarvestedByOpp[dirIndex]++;
                else
                    keepCountingOpp = false;
            }
        }

        public void foundHarvested(int x, int y, int width, int height, char[][] grid, List<List<Integer>> coords, char type) {
            for (int i = 1, steps = min(maxSteps(x, y, width, height), 3); i <= steps; i++) {
                int x1 = x + i * dx;
                int y1 = y + i * dy;

                if (grid[x1][y1] == type) {
                    coords.get(dirIndex).add(x1);
                    coords.get(dirIndex).add(y1);
                } else {
                    break;
                }
            }
        }

        protected abstract int maxSteps(int x, int y, int width, int height);
    }

    private static class ProcessResult {
        float priority;
        // 0 - south to north
        // 1 - south-west to north-east
        // 2 - east to west
        // 3 - south-east to north-west
        int[] canBeHarvestedByMe = new int[4];
        int[] canBeHarvestedByOpp = new int[4];

        void reset() {
            priority = 0;
            for (int i = 0; i < 4; i++) {
                canBeHarvestedByMe[i] = 0;
                canBeHarvestedByOpp[i] = 0;
            }
        }
    }
}