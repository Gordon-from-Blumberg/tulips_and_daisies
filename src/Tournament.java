import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Tournament {
    private List<Bot> bots;
    private Map<String, BotResult> resultMap = new HashMap<>();

    private String maxGoldPerPartyBot;
    private int maxGoldPerParty;

    public void setBots(List<Bot> bots) {
        this.bots = bots;

        resultMap.clear();
        for (Bot bot : bots) {
            resultMap.put(bot.getName(), new BotResult(bot.getName()));
        }

        maxGoldPerPartyBot = null;
        maxGoldPerParty = 0;
    }

    public Result run() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < bots.size(); i++) {
            for (int j = i + 1; j < bots.size(); j++) {
                runParties(bots.get(i), bots.get(j));
            }
        }
        System.out.println(String.format("tournament took %dms", System.currentTimeMillis() - start));

        Result result = new Result();
        result.maxGoldPerPartyBot = maxGoldPerPartyBot;
        result.maxGoldPerParty = maxGoldPerParty;
        result.botResults = resultMap.values().stream()
                .sorted((r1, r2) -> r1.victories == r2.victories
                        ? r2.gold - r1.gold
                        : r2.victories - r1.victories)
                .collect(Collectors.toList());

        return result;
    }

    private void runParties(Bot b1, Bot b2) {
        BotResult r1 = resultMap.get(b1.getName());
        int victories1 = r1.victories;
        int gold1 = r1.gold;
        BotResult r2 = resultMap.get(b2.getName());
        int victories2 = r2.victories;
        int gold2 = r2.gold;

        runDoubleParty(b1, b2);
        runDoubleParty(b1, b2);

        System.out.println(String.format("%s vs %s\t%d:%d\t%d:%d", b1.getName(), b2.getName(),
                r1.victories - victories1, r2.victories - victories2,
                r1.gold - gold1, r2.gold - gold2));
    }

    private void runDoubleParty(Bot b1, Bot b2) {
        Game game = new Game(System.currentTimeMillis() % 942801);

        game.init(b1, b2);
        processResult(game.run());

        game.swapBots();
        processResult(game.run());
    }

    private void processResult(Game.Result gameResult) {
        if (gameResult.winner != null) {
            BotResult winnerResult = resultMap.get(gameResult.winner.getName());
            winnerResult.victories++;
            winnerResult.gold += gameResult.winnerGold;
//            resultMap.get(gameResult.second.getName()).gold += gameResult.secondGold;
        } else {
            resultMap.get(gameResult.bots[0].getName()).gold += gameResult.winnerGold;
            resultMap.get(gameResult.bots[1].getName()).gold += gameResult.secondGold;
        }

        if (gameResult.winnerGold > maxGoldPerParty) {
            maxGoldPerPartyBot = (gameResult.winner != null ? gameResult.winner : gameResult.bots[0]).getName();
            maxGoldPerParty = gameResult.winnerGold;
        }
    }

    static class BotResult {
        private String name;
        private int victories;
        private int gold;

        private BotResult(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public int getVictories() {
            return victories;
        }

        public int getGold() {
            return gold;
        }
    }

    static class Result {
        private List<BotResult> botResults;
        private String maxGoldPerPartyBot;
        private int maxGoldPerParty;

        public List<BotResult> getBotResults() {
            return botResults;
        }

        public String getMaxGoldPerPartyBot() {
            return maxGoldPerPartyBot;
        }

        public int getMaxGoldPerParty() {
            return maxGoldPerParty;
        }
    }
}
