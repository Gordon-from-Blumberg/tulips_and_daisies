import java.util.*;

public class Game {
    private final Map<Character, Integer> costMap = new HashMap<>();

    private final int width;
    private final int height;

    private final char[][] initialGrid;
    private final char[][] grid;

    private final Bot[] bots = new Bot[2];
    private final int[] gold = new int[2];
    private final char[] flowerTypes = new char[] {'T', 'D'};

    private int turns;
    private int currentBotIndex;
    private boolean finished;

    private Result result;

    {
        costMap.put('S', 0);
        costMap.put('G', 1);
        costMap.put('R', 5);
        costMap.put('T', 10);
        costMap.put('D', 10);
    }

    public Game(long seed) {
        Random random = new Random(seed);

        width = randomInt(random, 8, 16);
        height = randomInt(random, 8, 16);

        initialGrid = new char[width][height];
        grid = new char[width][height];

        generateGrid(random);
    }

    public Game(String[] grid) {
        height = grid.length;
        width = grid[0].length();

        initialGrid = new char[width][height];
        this.grid = new char[width][height];

        for (int y = 0; y < height; y++) {
            String row = grid[y];
            for (int x = 0; x < width; x++) {
                initialGrid[x][y] = row.charAt(x);
            }
        }
    }

    public void init(Bot bot1, Bot bot2) {
        copyGrid();

        bots[0] = bot1;
        bots[1] = bot2;

        gold[0] = gold[1] = 100;

        bot1.init(width, height,
                costMap.get('S'), costMap.get('G'), costMap.get('R'), costMap.get('D'),
                "tulips", "daisies");
        bot2.init(width, height,
                costMap.get('S'), costMap.get('G'), costMap.get('R'), costMap.get('T'),
                "daisies", "tulips");

        turns = 256;
        currentBotIndex = 0;
        finished = false;

        result = new Result();
        result.bots = bots;
    }

    public Result run() {
        while (!finished)
            turn();

        return result;
    }

    public void swapBots() {
        if (!finished) throw new IllegalStateException("Game have not yet finished!");

        init(bots[1], bots[0]);
    }

    void turn() {
        String[] grid = new String[height];
        for (int y = 0; y < height; y++) {
            grid[y] = "";
            for (int x = 0; x < width; x++) {
                grid[y] += this.grid[x][y];
            }
        }

        String action = bots[currentBotIndex].runTurn(turns, gold[currentBotIndex], gold[opp()], grid);
//        System.out.println(String.format("turn of bot %d is %s", currentBotIndex, action));
        String[] coords = action.split(" ");

        int x = Integer.parseInt(coords[1]);
        int y = Integer.parseInt(coords[0]);

        if (isValid(x, y)) {

            process(x, y);

            if (--turns == 0)
                end();
            else
                currentBotIndex = opp();

        } else {
            end();
        }
    }

    private void process(int x, int y) {
        gold[currentBotIndex] -= costMap.get(grid[x][y]);
        grid[x][y] = flowerTypes[currentBotIndex];

        List<List<Integer>> harvestedFlowers = new ArrayList<>(4);
        for (int i = 0; i < 4; i++)
            harvestedFlowers.add(new ArrayList<>());

        for (Bot.Direction dir : Bot.Direction.values()) {
            dir.foundHarvested(x, y, width, height, grid, harvestedFlowers, flowerTypes[currentBotIndex]);
        }

        int harvestedCount = 0;
        for (List<Integer> dirCoords : harvestedFlowers) {
            if (dirCoords.size() >= 2 * 3) {
                for (int i = 0; i < dirCoords.size(); i += 2) {
                    grid[dirCoords.get(i)][dirCoords.get(i + 1)] = 'G';
                    harvestedCount++;
                }
            }
        }

        if (harvestedCount > 0) {
            grid[x][y] = 'G';
            int earned = Bot.getHarvestedCost(harvestedCount + 1);
            gold[currentBotIndex] += earned;
            Bot.debug("bot %d harvested %d flowers and earned %d gold", currentBotIndex, harvestedCount + 1, earned);
        }
    }

    private void end() {
        result.turn = 256 - turns;
        result.winner = turns > 0
                ? bots[opp()] : gold[0] > gold[1]
                ? bots[0] : gold[0] < gold[1]
                ? bots[1] : null;

        result.second = result.winner == null
                ? null : result.winner == bots[0]
                ? bots[1] : bots[0];

        result.winnerGold = result.winner == bots[0] ? gold[0] : gold[1];
        result.secondGold = result.winner == bots[0] ? gold[1] : gold[0];

        finished = true;
    }

    private int opp() {
        return currentBotIndex == 0 ? 1 : 0;
    }

    private boolean isValid(int x, int y) {
        if (x < 0 || x >= width) return false;
        if (y < 0 || y >= height) return false;
        return costMap.get(grid[x][y]) <= gold[currentBotIndex];
    }

    private void generateGrid(Random random) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                initialGrid[x][y] = 'S';
            }
        }

        int rocks = randomInt(random, (width * height) / 20, (width * height) / 10);
        while (rocks-- > 0) {
            boolean added = false;
            while (!added) {
                int x = random.nextInt(width);
                int y = random.nextInt(height);
                if (initialGrid[x][y] != 'R') {
                    initialGrid[x][y] = 'R';
                    added = true;
                }
            }
        }
    }

    private void copyGrid() {
        for (int i = 0; i < initialGrid.length; i++) {
            System.arraycopy(initialGrid[i], 0, grid[i], 0, height);
        }
    }

    private int randomInt(Random random, int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }

    Result getResult() {
        return result;
    }

    int getGold(int index) {
        return gold[index];
    }

    public static class Result {
        Bot[] bots;
        Bot winner;
        Bot second;
        int winnerGold;
        int secondGold;
        int turn;
    }
}
