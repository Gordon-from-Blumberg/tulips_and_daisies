public class Test {
    public static void main(String[] args) {
        checkInt(7, Bot.getHarvestedCost(4));
        checkInt(20, Bot.getHarvestedCost(6));

        Bot testBot = new Bot();
        testBot.setAllConfigParams(1f);
        testBot.init(7, 7, 0, 1, 5, 10, "t", "d");

        String[] grid = new String[] {
                "SSSTTTD",
                "DSSSSTT",
                "DSSSTST",
                "GSSTSST",
                "DSSTSSS",
                "SSSTSSS",
                "STTSTSS"
        };

        checkBotTurn(testBot, 100, grid, 6, 0, -10 + 9 - 9 + Bot.getHarvestedCost(10));
        checkBotTurn(testBot, 9, grid, 3, 6, 0 + 15 - 6 + Bot.getHarvestedCost(7));

        testBot.config.params[Bot.HARVESTED_EARNED_BY_OPP_COEF_INDEX] = 10;
        checkBotTurn(testBot, 9, grid, 0, 3, -1 + 15 + Bot.getHarvestedCost(4) * 10);

        Game testGame = new Game(grid);
        Bot bot1 = new Bot() {
            @Override
            public String runTurn(int turnsLeft, int gold, int oppGold, String[] grid) {
                return "0 6";
            }
        };
        Bot bot2 = new Bot() {
            @Override
            public String runTurn(int turnsLeft, int gold, int oppGold, String[] grid) {
                return "-1 0";
            }
        };
        testGame.init(bot1, bot2);

        checkInt(100, testGame.getGold(0));
        checkInt(100, testGame.getGold(1));
        checkGameResult(null, null, 0, 0, 0, testGame.getResult());
        testGame.turn();

        checkInt(100 - 10 + Bot.getHarvestedCost(10), testGame.getGold(0));
        checkInt(100, testGame.getGold(1));
        checkGameResult(null, null, 0, 0, 0, testGame.getResult());
        testGame.turn();

        checkGameResult(bot1, bot2, 100 - 10 + Bot.getHarvestedCost(10), 100, 1, testGame.getResult());
    }

    private static void checkInt(int expected, int actual) {
        if (expected != actual)
            throw new AssertionError(String.format("Expected %d, but got %d", expected, actual));
    }

    private static void checkString(String expected, String actual) {
        if (!expected.equals(actual))
            throw new AssertionError(String.format("Expected %s, but got %s", expected, actual));
    }

    private static void checkBotTurn(Bot bot, int gold, String[] grid, int expectedX, int expectedY, float expectedPriority) {
        String action = bot.runTurn(256, gold, 100, grid);
        checkString(String.format("%d %d %.3f", expectedY, expectedX, expectedPriority), action);
    }

    private static void checkGameResult(Bot expectedWinner, Bot expectedSecond,
                                        int expectedWinnerGold, int expectedSecondGold, int expectedTurn,
                                        Game.Result actual) {

        if (expectedWinner != actual.winner)
            throw new AssertionError("Winner does not equal to expected");
        if (expectedSecond != actual.second)
            throw new AssertionError("Second does not equal to expected");

        checkInt(expectedWinnerGold, actual.winnerGold);
        checkInt(expectedSecondGold, actual.secondGold);
        checkInt(expectedTurn, actual.turn);
    }
}
