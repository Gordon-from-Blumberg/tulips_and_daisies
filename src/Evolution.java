import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Evolution {
    private static final String WORK_DIR = "generation";
    private static final String GEN_DIR_PREFIX = "gen_";

    private Generator generator;

    private List<Bot.Config> generation;
    private List<Bot> bots = new ArrayList<>();

    public static void main(String[] args) {
        Evolution evolution = new Evolution();

        evolution.run(new Tournament(), 100);
    }

    public Evolution() {
        File workDir = new File(WORK_DIR);
        if (workDir.exists()) {
            String[] gens = workDir.list((d, f) -> f.startsWith(GEN_DIR_PREFIX));
            if (gens != null) {
                Arrays.sort(gens);

                String lastGen = gens[gens.length - 1];
                int lastGenNumber = Integer.parseInt(lastGen.substring(GEN_DIR_PREFIX.length()));
                int lastId = -1;
                File lastGenDir = new File(workDir, lastGen);

                File[] cfgFiles = lastGenDir.listFiles((d, f) -> f.matches("g\\d{4}-\\d{4}-[\\w,]+-\\d{5}"));

                if (cfgFiles != null) {

                    generation = new ArrayList<>();
                    for (File cfgFile : cfgFiles) {
                        System.out.println(String.format("Process file %s", cfgFile.getName()));

                        int id = Generator.extractId(cfgFile.getName());
                        if (id > lastId)
                            lastId = id;

                        Bot.Config config = new Bot.Config(Bot.PARAM_COUNT);
                        config.id = cfgFile.getName();
                        try (Scanner scanner = new Scanner(cfgFile)) {

                            while (scanner.hasNext()) {
                                int paramIndex = scanner.nextInt();
                                config.params[paramIndex] = scanner.nextFloat();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        generation.add(config);
                    }

                    generator = new Generator(Bot.PARAM_COUNT, lastGenNumber, lastId);
                }
            }
        }

        if (generator == null) {
            generator = new Generator(Bot.PARAM_COUNT);
            generation = generator.createFirstGeneration();
            writeGeneration();
            writeStatistic();
        }

        updateBots();
    }

    void run(Tournament tournament, int genCount) {
        while (genCount-- > 0) {
            tournament.setBots(bots);
            Tournament.Result result = tournament.run();
            writeResult(result);

            nextGeneration(findWinners(result, 3));
        }
    }

    private void nextGeneration(List<Bot.Config> winners) {
        generation = generator.nextGeneration(winners);

        writeGeneration();
        writeStatistic();

        updateBots();
    }

    private List<Bot.Config> findWinners(Tournament.Result result, int count) {
        List<Bot.Config> winners = new ArrayList<>();
        System.out.println("Winners: ");
        for (int i = 0; i < count; i++) {
            String name = result.getBotResults().get(i).getName();
            winners.add(findById(name));
            System.out.println(name);
        }
        System.out.println();
        return winners;
    }

    private Bot.Config findById(String id) {
        for (Bot.Config cfg : generation) {
            if (cfg.id.equals(id)) {
                return cfg;
            }
        }
        return null;
    }

    private void writeGeneration() {
        String genDirPath = String.format("%s/gen_%04d", WORK_DIR, generator.getGenerationNumber());
        File genDir = new File(genDirPath);
        if (!genDir.exists())
            genDir.mkdirs();

        try {
            for (Bot.Config cfg : generation) {
                File file = new File(genDir, cfg.id);
                if (!file.exists())
                    file.createNewFile();

                FileWriter fw = new FileWriter(file);
                for (int i = 0; i < Bot.PARAM_COUNT; i++) {
                    fw.write(String.format("%2d\t%+02.3f\n", i, cfg.params[i]));
                }

                fw.flush();
                fw.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeStatistic() {
        File genDir = new File(String.format("%s/gen_%04d", WORK_DIR, generator.getGenerationNumber()));
        if (!genDir.exists())
            genDir.mkdirs();

        try {
            File file = new File(genDir, String.format("gen_%04d-statistic", generator.getGenerationNumber()));
            if (!file.exists())
                file.createNewFile();

            FileWriter fw = new FileWriter(file);
            for (int i = 0; i < Bot.PARAM_COUNT; i++) {
                float min = 0;
                float max = 0;
                float sum = 0;
                for (int j = 0; j < generation.size(); j++) {
                    float par = generation.get(j).params[i];
                    if (j == 0) {
                        min = par;
                        max = par;
                    } else {
                        if (par < min) min = par;
                        if (par > max) max = par;
                    }
                    sum += par;
                }

                float avg = sum / generation.size();
                float sko = 0;

                for (int j = 0; j < generation.size(); j++) {
                    float par = generation.get(j).params[i];
                    float d = par - avg;
                    sko += d * d;
                }

                fw.write(String.format("%+02.3f\t%+02.3f\t%+02.3f\t%02.3f\n", min, avg, max, sko));
            }

            fw.flush();
            fw.close();

        } catch (IOException e) {
             e.printStackTrace();
        }
    }

    private void writeResult(Tournament.Result result) {
        File genDir = new File(String.format("%s/gen_%04d", WORK_DIR, generator.getGenerationNumber()));
        if (!genDir.exists())
            genDir.mkdirs();

        try {
            File file = new File(genDir, String.format("gen_%04d-tournament", generator.getGenerationNumber()));
            if (!file.exists())
                file.createNewFile();

            FileWriter fw = new FileWriter(file);

            for (Tournament.BotResult botRes : result.getBotResults()) {
                fw.write(String.format("%s\t%2d\t%7d\n", botRes.getName(), botRes.getVictories(), botRes.getGold()));
            }

            fw.write(String.format("Max gold per party: %s -> %7d", result.getMaxGoldPerPartyBot(), result.getMaxGoldPerParty()));

            fw.flush();
            fw.close();

            File winnerFile = new File(genDir, String.format("gen_%04d-winner", generator.getGenerationNumber()));
            if (!file.exists())
                file.createNewFile();

            Bot.Config winner = findById(result.getBotResults().get(0).getName());

            if (winner != null) {
                fw = new FileWriter(winnerFile);

                for (float par : winner.params) {
                    fw.write(String.format("%+2.3ff,\n", par));
                }

                fw.flush();
                fw.close();
            }

        } catch (IOException e) {
             e.printStackTrace();
        }
    }

    private void updateBots() {
        for (int i = 0; i < generation.size(); i++) {
            if (bots.size() == i)
                bots.add(new Bot());

            bots.get(i).config = generation.get(i);
        }
    }
}
